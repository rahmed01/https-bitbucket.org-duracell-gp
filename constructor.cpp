#include "window.h"
#include "link.h"

int window::Window(LPCWSTR Title)
{
hDC=NULL;
hRC=NULL;
hWnd=NULL;


active=0;
bool done = 0;
MSG	msg;

WindowGL(Title,600,400,32);

	while(!done)
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	
		{
			if (msg.message==WM_QUIT)
								done=TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (!FrontEnd())
								done=TRUE;	
			else	
				SwapBuffers(hDC);
		}
	}

	KillWindow();
	return(msg.wParam);
}