#include "window.h"
#include "link.h"

GLvoid window::WindowResize(int width, int height)
{
	if (height==0)
					height=1;

	glViewport(0,0,width,height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
}