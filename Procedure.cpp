#include "window.h"
#include "link.h"


bool CALLBACK window::StaticProc(HWND hWnd,	UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
			}
			break;
		}

		case WM_CLOSE:
		{
			PostQuitMessage(0);	
			return 0;
		}
		case WM_LBUTTONDOWN:
		{
			if (LOWORD(lParam) < 640 && HIWORD(lParam) < 30 )
			{
			   ReleaseCapture();
			   SendMessage (hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0);
			}

			if (LOWORD(lParam) < 640 && LOWORD(lParam) > 610 && HIWORD(lParam) < 30 )
			{
				KillWindow();
			}
			return 0;
		}
	}

	return WndProc(uMsg,  wParam,  lParam);
}

LRESULT window::WndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
			}
			break;
		}

		case WM_CLOSE:
		{
			PostQuitMessage(0);	
			return 0;
		}
		case WM_LBUTTONDOWN:
		{
			if (LOWORD(lParam) < 640 && HIWORD(lParam) < 30 )
			{
			   ReleaseCapture();
			   SendMessage (hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0);
			}

			if (LOWORD(lParam) < 640 && LOWORD(lParam) > 610 && HIWORD(lParam) < 30 )
			{
			  // KillWindow();
			   
			}
			return 0;
		}
	}

	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}