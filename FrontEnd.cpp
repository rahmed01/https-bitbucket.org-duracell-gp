#include "window.h"
#include "link.h"

int window::FrontEnd(GLvoid)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();		
	/*glTranslatef(-1.5f,0.0f,-6.0f);						// Move Left 1.5 Units And Into The Screen 6.0
	glBegin(GL_TRIANGLES);	
		glColor3f(1.0f,1.0f,1.0f);
		glVertex3f( 0.0f, 1.0f, 0.0f);					// Top
		glVertex3f(-1.0f,-1.0f, 0.0f);					// Bottom Left
		glVertex3f( 1.0f,-1.0f, 0.0f);					// Bottom Right
	glEnd();											// Finished Drawing The Triangle
	glTranslatef(3.0f,0.0f,-0.5f);						// Move Right 3 Units
	glBegin(GL_QUADS);									// Draw A Quad
		glVertex3f(-1.0f, 1.0f, 0.0f);					// Top Left
		glVertex3f( 1.0f, 1.0f, 0.0f);					// Top Right
		glVertex3f( 1.0f,-1.0f, 0.0f);					// Bottom Right
		glVertex3f(-1.0f,-1.0f, 0.0f);					// Bottom Left
	glEnd();
	*/
	glTranslatef(-0.0f,0.0f,-1.0f);
	glBegin(GL_QUADS);
		glColor3f(0.0f,0.0f,1.0f);
		glVertex3f( -0.62f, 0.45f, 0.0f);
		glVertex3f(  0.62f, 0.45f, 0.0f);
		glVertex3f(  0.62f, 0.35f, 0.0f);
		glVertex3f( -0.62f, 0.35f, 0.0f);
	glEnd();
	return true;
}